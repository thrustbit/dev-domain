<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Domain\User\Model;

use Thrustbit\DevDomain\Application\Values\Entity;
use Thrustbit\DevDomain\Domain\User\Model\Concern\LocalUser;
use Thrustbit\DevDomain\Domain\User\Model\Contracts\LocalUser as LocalUserContract;
use Thrustbit\ModelEvent\ModelRoot;

abstract class UserAggregate extends ModelRoot implements LocalUserContract, Entity
{
    use LocalUser;

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'user_name', 'email', 'password', 'activated', 'throttled'
    ];

    /**
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;
}
<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Domain\User\Model\Contracts;

use Thrustbit\DevDomain\Application\Values\Contracts\EncodedPassword;

interface LocalUser extends User
{
    public function getPassword(): EncodedPassword;
}
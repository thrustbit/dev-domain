<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Domain\User\Model\Contracts;

use Thrustbit\DevDomain\Application\Values\Contracts\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Contracts\UserName;
use Thrustbit\DevDomain\Application\Values\Contracts\Uuid;
use Thrustbit\DevDomain\Application\Values\Identifier;

interface User
{
    public function getId(): Uuid;

    public function getIdentifier(): Identifier;

    public function getUserName(): UserName;

    public function getEmail(): EmailAddress;

    public function getRoles(): array;
}
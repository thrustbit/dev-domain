<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Domain\User\Model\Concern;

use Thrustbit\DevDomain\Application\Values\Contracts\EncodedPassword;
use Thrustbit\DevDomain\Application\Values\Credentials\BcryptPassword;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Contracts\EmailAddress as EmailAddressContract;
use Thrustbit\DevDomain\Application\Values\Contracts\UserName as UserNameContract;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;
use Thrustbit\DevDomain\Application\Values\Contracts\Uuid;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;

trait LocalUser
{

    protected function aggregateId(): string
    {
        return $this->getId()->identify();
    }

    public function getId(): Uuid
    {
        if ($this->id instanceof Uuid) {
            return $this->id;
        }

        return UserId::fromString($this->getKey());
    }

    public function getIdentifier(): Identifier
    {
        return $this->getEmail();
    }

    public function getUserName(): UserNameContract
    {
        if ($this->userName instanceof UserName) {
            return $this->user_name;
        }

        return UserName::fromString($this->user_name);
    }

    public function getEmail(): EmailAddressContract
    {
        if ($this->email instanceof EmailAddressContract) {
            return $this->email;
        }

        return EmailAddress::fromString($this->email);
    }

    public function getPassword(): EncodedPassword
    {
        if ($this->password instanceof EncodedPassword) {
            return $this->password;
        }

        return BcryptPassword::fromString($this->password);
    }
}
<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Domain\User\Model;

use Thrustbit\DevDomain\Application\Values\Entity;

class LocalUser extends UserAggregate
{

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this
            && $this->getId()->sameValueAs($aEntity->getId());
    }

    public function getRoles(): array
    {
        if (null !== $this->roles) {
            return $this->roles;
        }

        return ['ROLE_USER'];
    }
}
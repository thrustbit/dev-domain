<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Domain\User\Services;

use Thrustbit\DevDomain\Application\Values\Contracts\UserName;

interface UniqueUserName
{
    public function __invoke(UserName $userName): bool;
}
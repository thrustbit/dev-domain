<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Domain\User\Services;

use Thrustbit\DevDomain\Application\Values\Contracts\EncodedPassword;
use Thrustbit\DevDomain\Application\Values\Credentials\ClearPassword;

interface PasswordEncoder
{
    public function __invoke(ClearPassword $clearPassword): EncodedPassword;
}
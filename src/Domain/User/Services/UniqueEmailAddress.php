<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Domain\User\Services;

use Thrustbit\DevDomain\Application\Values\Contracts\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Contracts\Uuid;

interface UniqueEmailAddress
{
    public function __invoke(EmailAddress $email): ?Uuid;
}
<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Exceptions;

use Assert\Assertion;

class Validate extends Assertion
{
    protected static $exceptionClass = ValidationFailed::class;
}
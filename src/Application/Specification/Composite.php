<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Specification;

abstract class Composite implements Specification
{

    public function andIsSatisfiedBy(Specification $spec)
    {
        return new AndSpecification($this, $spec);
    }
}
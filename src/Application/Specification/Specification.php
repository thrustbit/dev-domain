<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Specification;

interface Specification
{
    public function isSatisfiedBy($specification): bool;
}
<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Specification;

class AndSpecification extends Composite
{
    /**
     * @var Specification
     */
    private $a;

    /**
     * @var Specification
     */
    private $b;

    public function __construct(Specification $a, Specification $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    public function isSatisfiedBy($candidate): bool
    {
        return $this->a->isSatisfiedBy($candidate) && $this->b->isSatisfiedBy($candidate);
    }
}
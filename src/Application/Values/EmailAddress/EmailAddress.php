<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values\EmailAddress;

use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Contracts\EmailAddress as Email;
use Thrustbit\DevDomain\Application\Values\Value;

class EmailAddress implements Email
{

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    protected static $message = 'Email address provided is not valid.';

    protected function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function fromString($email): self
    {
        Validate::string($email, self::$message);
        Validate::email($email, self::$message);

        return new self($email);
    }

    public function identify(): string
    {
        return $this->email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->email === $aValue->getEmail();
    }

    public function __toString(): string
    {
        return $this->email;
    }
}
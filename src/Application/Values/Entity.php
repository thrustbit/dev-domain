<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values;

interface Entity
{
    public function sameIdentityAs(Entity $aEntity): bool;
}
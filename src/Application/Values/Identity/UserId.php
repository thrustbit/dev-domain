<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values\Identity;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Contracts\Uuid as UuidContract;
use Thrustbit\DevDomain\Application\Values\Value;

class UserId implements UuidContract
{
    /**
     * @var UuidInterface
     */
    private $uid;

    private function __construct(UuidInterface $uid)
    {
        $this->uid = $uid;
    }

    public static function nextIdentity(): self
    {
        return new self(Uuid::uuid4());
    }

    public static function fromString($uid): self
    {
        $message = 'User id is not valid';

        Validate::string($uid, $message);
        Validate::notEmpty($uid, $message);
        Validate::uuid($uid, $message);

        return new self(Uuid::fromString($uid));
    }

    public function getUid(): UuidInterface
    {
        return $this->uid;
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->uid->equals($aValue->getUid());
    }

    public function identify()
    {
        return $this->uid->toString();
    }

    public function __toString(): string
    {
        return $this->identify();
    }
}
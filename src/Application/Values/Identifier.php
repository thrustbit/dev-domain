<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values;

interface Identifier extends Value
{
    public function identify();
}
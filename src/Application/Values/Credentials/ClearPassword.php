<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values\Credentials;

use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Value;

class ClearPassword extends Password
{
    public static function fromStringWithConfirmation($password, $confirmedPassword): ClearPassword
    {
        Validate::same($password, $confirmedPassword, 'Password and his confirmation does not match.');

        return new self($password);
    }

    public static function fromString($password): ClearPassword
    {
        return new self($password);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof ClearPassword && $this->password === $aValue->toString();
    }

    public function getCredentials()
    {
        return $this->password;
    }
}
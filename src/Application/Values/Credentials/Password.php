<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values\Credentials;

use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Contracts\Credentials;

abstract class Password implements Credentials
{
    const MIN_LENGTH = 8;
    const MAX_LENGTH = 255;

    /**
     * @var string
     */
    protected $password;

    protected function __construct($password)
    {
        $this->validatePassword($password);

        $this->password = $password;
    }

    protected function validatePassword($password): void
    {
        Validate::string($password, 'Password is invalid.');
        Validate::betweenLength($password, self::MIN_LENGTH, self::MAX_LENGTH,
            'Password must be between' . self::MIN_LENGTH . ' and ' . self::MAX_LENGTH . ' characters');
    }

    public function toString(): string
    {
        return $this->password;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
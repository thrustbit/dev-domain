<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values\Credentials;

use Thrustbit\DevDomain\Application\Values\Contracts\EncodedPassword;
use Thrustbit\DevDomain\Application\Values\Value;

class BcryptPassword extends Password implements EncodedPassword
{

    public static function fromString($password): self
    {
        return new self($password);
    }

    public function getCredentials(): string
    {
        return $this->password;
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->getCredentials() === $aValue->getCredentials();
    }
}
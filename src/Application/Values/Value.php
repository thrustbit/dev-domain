<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values;

interface Value
{
    public function sameValueAs(Value $aValue): bool;
}
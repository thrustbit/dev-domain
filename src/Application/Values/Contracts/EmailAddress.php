<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values\Contracts;

use Thrustbit\DevDomain\Application\Values\Identifier;

interface EmailAddress extends Identifier
{
}
<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values;

use MabeEnum\EnumSerializableTrait;

abstract class Enum extends \MabeEnum\Enum implements \Serializable, Value
{
    use EnumSerializableTrait;

    public function toString(): string
    {
        return $this->getName();
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $this->is($aValue);
    }
}
<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values\UserName;

use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Contracts\UserName as UserNameContract;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Value;

class UserName implements UserNameContract, Identifier
{
    const MIN_LENGTH = 3;
    const MAX_LENGTH = 150;

    /**
     * @var string
     */
    private $userName;

    private function __construct(string $userName)
    {
        $this->userName = $userName;
    }

    public static function fromString($userName): self
    {
        Validate::string($userName, 'User name is not valid');
        Validate::betweenLength($userName, self::MIN_LENGTH, self::MAX_LENGTH,
            sprintf('User name must be between %s and %s characters', self::MIN_LENGTH, self::MAX_LENGTH)
        );

        return new self($userName);
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function toString(): string
    {
        return $this->getUserName();
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->userName === $aValue->getUserName();
    }

    public function identify()
    {
        return $this->toString();
    }

    public function __toString(): string
    {
        return $this->getUserName();
    }
}
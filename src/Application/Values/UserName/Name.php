<?php

declare(strict_types=1);

namespace Thrustbit\DevDomain\Application\Values\UserName;

use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Contracts\UserName;
use Thrustbit\DevDomain\Application\Values\Value;

class Name implements UserName
{

    const MIN_LENGTH = 3;
    const MAX_LENGTH = 150;
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    private function __construct(string $firstName, string $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public static function fromString($firstName, $lastName): self
    {
        Validate::string($firstName);
        Validate::string($lastName);

        Validate::betweenLength($firstName . $lastName, self::MIN_LENGTH, self::MAX_LENGTH);

        return new self($firstName, $lastName);
    }

    public function getName(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function toString(): string
    {
        return $this->getName();
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->getName() === $aValue->getName();
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}